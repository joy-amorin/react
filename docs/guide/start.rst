===========
Quick start
===========

First you need to install a Lino :term:`developer environment` as described in
the :ref:`lino.dev`.

Then here we go::

    $ sudo apt install nodejs npm

    $ npm install

    $ go react
    $ mkdir -p lino_react/react/static/media

    $ npm run build

Now let's do a local change and test it.

TODO
