==================
The lino-react app
==================

Here we introduce all the ins and outs of JavaScript features
that makes the lino-react, for what it is, a good WebUI tool for
displaying data with just a few tweaks in a lino plugin level config.

Before continuing onto further detail we assume that you have a good
understanding of JavaScript and ReactJS framework.


.. toctree::
    :maxdepth: 1

    App
    NavigationControl
    Base
