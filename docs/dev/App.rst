=================================
The main entry module for the app
=================================

source: lino_react/react/components/App

As a whole this module contains basic browser routing, delegating data render and dialog
render, showing toast and showing error messages features.

.. js:autofunction:: LinoRouter

.. js:autoclass:: App
    :members:

.. js:autointerface:: ServerErrorProps
    :members:

.. js:autofunction:: InternalServerError
