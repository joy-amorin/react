"""
.. autosummary::
   :toctree:

    react

"""

from .setup_info import SETUP_INFO

__version__ = SETUP_INFO['version']

srcref_url = 'https://gitlab.com/lino-framework/react/blob/master/%s'
# doc_trees = []
intersphinx_urls = dict(docs="https://lino-framework.gitlab.io/react/")
