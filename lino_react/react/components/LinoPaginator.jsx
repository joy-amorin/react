export const name = "LinoPaginator";

import React from "react";
import * as constants from './constants';
import { RegisterImportPool, Component, URLContextType } from "./Base";

let ex; const exModulePromises = ex = {
    prButton: import(/* webpackChunkName: "prButton_LinoPaginator" */"primereact/button"),
    prDropDown: import(/* webpackChunkName: "prDropDown_LinoPaginator" */"primereact/dropdown"),
    prOverlayPanel: import(/* webpackChunkName: "prOverlayPanel_LinoPaginator" */"primereact/overlaypanel"),
    prPaginator: import(/* webpackChunkName: "prPaginator_LinoPaginator" */"primereact/paginator"),
    prSlider: import(/* webpackChunkName: "prSlider_LinoPaginator" */"primereact/slider"),
    prInputNumber: import(/* webpackChunkName: "prInputNumber_LinoPaginator" */"primereact/inputnumber"),
    u: import(/* webpackChunkName: "LinoUtils_LinoPaginator" */"./LinoUtils"),
    i18n: import(/* webpackChunkName: "i18n_LinoPaginator" */"./i18n"),
}
RegisterImportPool(ex);


export class LinoPaginator extends Component {
    static requiredModules = ["prButton", "prDropDown", "prOverlayPanel",
        "prPaginator", "prSlider", "u", "i18n", "prInputNumber"];
    static iPool = ex;
    static contextType = URLContextType;

    async prepare() {
        await super.prepare();
        this.ex.i18n = this.ex.i18n.default;
    }

    constructor(props, context) {
        super(props);
        this.state = {
            ...this.state,
            slider: props.slider || false,
            sliderDisabled: false,
            showLimit: false,
            sliderValue: 1
        };

        this.findRowDiff = this.findRowDiff.bind(this);
        this.getRowsPerPage = this.getRowsPerPage.bind(this);
        this.setRowsPerPage = this.setRowsPerPage.bind(this);
    }

    findRowDiff(value) {
        if (value === 0) return 0;
        let ttl = this.context.controller.dataContext.mutableContext.count,
            lmt = this.context[constants.URL_PARAM_LIMIT],
            oldValue;
        if (lmt > ttl) lmt = ttl;
        oldValue = lmt / ttl;
        if (value > 0) return Math.round((ttl - lmt) ** value);
        if (value < oldValue) return Math.round(lmt ** (value * -1)) * -1;
        return 0
    }

    getRowsPerPage(value) {
        let rowsPerPage = this.context[constants.URL_PARAM_LIMIT] + this.findRowDiff(value);
        if (rowsPerPage === 0) {
            this.setState({sliderValue: 1});
            window.App.toast.show({severity: "warn", summary: "URL_PARAM_LIMIT = 0; (not allowed!)"});
            return null;
        }
        if (rowsPerPage === this.context[constants.URL_PARAM_LIMIT]) {
            this.setState({sliderValue: 1});
            return null;
        }
        return rowsPerPage;
    }

    setRowsPerPage(rowsPerPage) {
        if (rowsPerPage == null) return;
        this.setState({sliderValue: 1});
        this.context.controller.history.replace({
            [constants.URL_PARAM_START]: 0,
            [constants.URL_PARAM_LIMIT]: rowsPerPage
        });
    }

    incrementDecrementButton(polarity=-1) {
        const limit = this.context[constants.URL_PARAM_LIMIT];
        const {count} = this.context.controller.dataContext.mutableContext;
        return <this.ex.prButton.Button
            disabled={polarity === -1 ? limit <= 1 : count <= limit}
            icon={`pi pi-${polarity === -1 ? "minus": "plus"}`}
            onClick={e => this.setRowsPerPage(limit + (1 * polarity))}
            type="button"/>
    }

    sliderToggleButton(iconOnly=false) {
        let label = "";
        if (!iconOnly) {
            const limit = this.context[constants.URL_PARAM_LIMIT];
            const count = this.context.controller.dataContext.mutableContext.rows.length;
            label = (this.state.showLimit
                ? (limit + this.findRowDiff(this.state.sliderValue - 1))
                : Math.min(limit, count)).toString();
        }
        return <this.ex.prButton.Button
            label={label}
            icon={iconOnly ? "pi pi-arrows-v": ""}
            onClick={e => {
                this.setState({showLimit: !this.state.showLimit});
                this.sliderPanel.toggle(e);
            }}
            type="button"/>
    }

    render() {
        if (!this.state.ready) return null;
        let c = this.context.controller,
            ad = c.static.actorData,
            ttl = c.dataContext.mutableContext.count;
        if (ad.preview_limit === 0 || ttl === 0 ||
            ttl === undefined || ad.simple_paginator ||
            this.context[constants.URL_PARAM_DISPLAY_MODE] == constants.DISPLAY_MODE_SUMMARY
        ) return null;
        const count = c.dataContext.mutableContext.rows.length;
        const lmt = this.context[constants.URL_PARAM_LIMIT];
        return <div ref={el => this.container = el}><this.ex.prPaginator.Paginator
            alwaysShow={true} rows={lmt} totalRecords={ttl}
            first={this.context[constants.URL_PARAM_START] || 0}
            template="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
            onPageChange={(e) => this.context.controller.history.replace({
                [constants.URL_PARAM_START]: e.page * lmt})}
            ref={el => this.pg = el}
            rightContent={
                ttl && <span
                    className={"l-grid-count"}
                    onKeyDown={e => {
                        if (['Home', 'End', 'Delete'].includes(e.code))
                            e.stopPropagation()}}>
                    {this.ex.i18n.t("Showing ")}
                    {this.state.slider
                        ? <React.Fragment>
                            {!this.state.showLimit && this.incrementDecrementButton(-1)}
                            {c.globals.isMobile && <>
                                {this.sliderToggleButton()}
                                {!this.state.showLimit && this.incrementDecrementButton(1)}
                            </>}
                            {!c.globals.isMobile && <>
                                <this.ex.prInputNumber.InputNumber
                                    allowEmpty={false} buttonLayout="horizontal"
                                    format={true} max={ttl} min={1}
                                    onKeyDown={e => {
                                        if (e.key === "Enter" || e.code === "Escape")
                                            document.activeElement.blur()}}
                                    onChange={e => this.nInput.style
                                        .width = `${this.nInput.value.length + 2}ch`}
                                    onValueChange={e => this.setRowsPerPage(e.value)}
                                    inputRef={ref => this.nInput = ref}
                                    inputStyle={{
                                        width: lmt > count
                                            ? `${(count.toString() + lmt.toString()).length + 12}ch`
                                            : `${lmt.toString().length + 2}ch`,
                                        fontFamily: '"Courier New", Courier, monospace'
                                    }}
                                    prefix={lmt !== count ? `${count} (&limit=` : ""}
                                    suffix={lmt !== count ? ")" : ""}
                                    useGrouping={false}
                                    value={lmt}/>
                                {this.incrementDecrementButton(1)}
                                {this.sliderToggleButton(true)}
                            </>}
                            <this.ex.prOverlayPanel.OverlayPanel
                                dismissable={true}
                                onHide={e => this.setState({showLimit: false})}
                                ref={el => this.sliderPanel = el}>
                                <this.ex.prSlider.Slider
                                    disabled={this.state.sliderDisabled}
                                    max={2} min={0}
                                    onChange={e => {
                                        if (e.originalEvent.type === "click") {
                                            this.setRowsPerPage(this.getRowsPerPage(e.value - 1));
                                        } else this.setState({sliderValue: e.value});
                                    }}
                                    onSlideEnd={e => this.setRowsPerPage(this.getRowsPerPage(e.value - 1))}
                                    orientation="vertical"
                                    step={1 / ttl}
                                    value={this.state.sliderValue}/>
                            </this.ex.prOverlayPanel.OverlayPanel>
                        </React.Fragment>
                        : <this.ex.prDropDown.Dropdown
                            style={{width: "80px"}}
                            value={lmt}
                            placeholder={lmt.toString()}
                            options={[15, 25, 50, 100, 200, 400, 800]}
                            onChange={(e) => {
                                this.setRowsPerPage(e.value);
                            }}/>
                        }
                    <span>{this.ex.i18n.t(" of ")}{ttl}</span>{this.ex.i18n.t(" rows")}
                </span>
            }
        /></div>
    }
}
